package com.chernyshov.yuriy.tcpclient;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    @SuppressWarnings("unused")
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private TCPClient mTcpClient;
    private EditText mInputMessageView;
    private EditText mIPAddressView;
    private EditText mPortView;
    private TextView mMessagesView;
    private TextView mConnectionStatusView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mInputMessageView = (EditText) findViewById(R.id.input_message_view);
        mIPAddressView = (EditText) findViewById(R.id.connect_ip_edit_view);
        mPortView = (EditText) findViewById(R.id.connect_port_edit_view);
        mMessagesView = (TextView) findViewById(R.id.messages_view);
        mConnectionStatusView = (TextView) findViewById(R.id.connection_state_view);
    }

    public void connectToServer(View view) {
        // connect to the server
        new ConnectTask().execute(mIPAddressView.getText().toString(),
                mPortView.getText().toString());
    }

    public void sendMessage(View view) {
        final String message = mInputMessageView.getText().toString();

        mMessagesView.append("\n<- " + message);

        //sends the message to the server
        if (mTcpClient != null) {
            mTcpClient.sendMessage(message);
        }

        mInputMessageView.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class ConnectTask extends AsyncTask<String, String, TCPClient> {

        @Override
        protected TCPClient doInBackground(String... message) {

            // TODO : Validate message here

            //we create a TCPClient object and
            mTcpClient = new TCPClient(message[0], Integer.valueOf(message[1]),
                    new TCPClient.TCPClientListener() {

                        @Override
                        public void onConnecting() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mConnectionStatusView.setText("Connecting ...");
                                }
                            });
                        }

                        @Override
                        public void onConnected() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mConnectionStatusView.setText("Connected");
                                }
                            });
                        }

                        @Override
                        //here the onMessageReceived method is implemented
                        public void onMessageReceived(String message) {
                            //this method calls the onProgressUpdate
                            publishProgress(message);
                        }
                    });
            mTcpClient.run();

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);

            mMessagesView.append("\n-> " + values[0]);
        }
    }
}