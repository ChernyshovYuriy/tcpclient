package com.chernyshov.yuriy.tcpclient;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 09.10.14
 * Time: 17:27
 */
public class TCPClient {

    private static final String LOG_TAG = TCPClient.class.getSimpleName();

    //Declare the interface. The method onMessageReceived(String message) will must be implemented in the MyActivity
    //class at on asynckTask doInBackground
    public interface TCPClientListener {

        public void onMessageReceived(String message);

        public void onConnecting();

        public void onConnected();
    }

    private String serverMessage;
    private String mServerIP = "127.0.0.1"; //your computer IP address
    private int mServerPort = 4444;
    private TCPClientListener mMessageListener = null;
    private boolean mRun = false;

    private PrintWriter out;
    private BufferedReader in;

    /**
     * Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TCPClient(String ipAddress, int ipPort, TCPClientListener listener) {
        mMessageListener = listener;
        mServerIP = ipAddress;
        mServerPort = ipPort;
    }

    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    public void sendMessage(String message) {
        if (out != null && !out.checkError()) {
            out.println(message);
            out.flush();
        }
    }

    public void stopClient() {
        mRun = false;
    }

    public void run() {

        mRun = true;

        try {
            //here you must put your computer's IP address.
            final InetAddress serverAddr = InetAddress.getByName(mServerIP);

            mMessageListener.onConnecting();

            Log.e(LOG_TAG, "Connecting...");

            //create a socket to make the connection with the server
            final Socket socket = new Socket(serverAddr, mServerPort);

            try {

                //send the message to the server
                out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())), true);

                mMessageListener.onConnected();

                //receive the message which the server sends back
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                //in this while the client listens for the messages sent by the server
                while (mRun) {
                    serverMessage = in.readLine();

                    if (serverMessage != null && mMessageListener != null) {
                        //call the method onMessageReceived from MyActivity class
                        mMessageListener.onMessageReceived(serverMessage);
                    }
                    serverMessage = null;
                }

                Log.e(LOG_TAG, "Received Message: '" + serverMessage + "'");

            } catch (Exception e) {

                Log.e(LOG_TAG, "Error:" + e.getMessage());

            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
            }

        } catch (Exception e) {

            Log.e(LOG_TAG, "C: Error", e);

        }
    }
}